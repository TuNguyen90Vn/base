package com.tn.base

import com.tn.base.di.DaggerAppComponent
import dagger.android.DaggerApplication

class App : DaggerApplication() {

    private val applicationInjector = DaggerAppComponent.builder().bindApplication(this).build()

    override fun applicationInjector() = applicationInjector

    override fun onCreate() {
        super.onCreate()
    }
}