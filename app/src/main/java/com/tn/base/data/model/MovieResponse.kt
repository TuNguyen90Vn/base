package com.tn.base.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.tn.base.presenter.model.Movie

data class MovieResponse(

    @SerializedName("page")
    @Expose
    val page: Int = 0,

    @SerializedName("total_results")
    @Expose
    val totalResults: Int = 0,

    @SerializedName("total_pages")
    @Expose
    val totalPages: Int = 0,

    @SerializedName("results")
    @Expose
    val results: List<MovieModel> = emptyList()
)

fun MovieResponse.transformTo(): List<Movie> {
    return results.map {
        Movie(
            id = it.id ?: 0,
            voteCount = it.voteCount ?: 0,
            voteAverage = it.voteAverage ?: 0.0,
            title = it.title ?: "",
            popularity = it.popularity ?: 0.0,
            posterPath = it.posterPath ?: "",
            originalLanguage = it.originalLanguage ?: "",
            backdropPath = it.backdropPath ?: "",
            overview = it.overview ?: ""
        )
    }
}