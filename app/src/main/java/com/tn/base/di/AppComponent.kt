package com.tn.base.di

import android.app.Application
import com.tn.base.di.builder.ActivityBuilder
import com.tn.base.di.provider.NetworkModule
import com.tn.base.di.provider.RepositoryModule
import com.tn.base.di.scopes.ApplicationScope
import com.tn.base.di.viewmodel.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule

@ApplicationScope
@Component(
    modules = [
        ApplicationModule::class,
        ActivityBuilder::class,
        NetworkModule::class,
        ViewModelModule::class,
        RepositoryModule::class,
        AndroidSupportInjectionModule::class
    ]
)
interface AppComponent : AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun bindApplication(application: Application): Builder

        fun build(): AppComponent
    }
}