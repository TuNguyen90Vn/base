package com.tn.base.di

import android.app.Application
import android.content.Context
import com.tn.base.di.scopes.ApplicationScope
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class ApplicationModule {

    @Binds
    @ApplicationScope
    abstract fun bindAppContext(application: Application): Context

}