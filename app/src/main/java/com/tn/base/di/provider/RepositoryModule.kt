package com.tn.base.di.provider

import com.tn.base.di.scopes.ApplicationScope
import com.tn.base.domain.MovieRepository
import com.tn.base.data.remote.MovieApi
import com.tn.base.domain.MovieRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    @ApplicationScope
    fun provideMovieRepository(service: MovieApi): MovieRepository {
        return MovieRepositoryImpl(service)
    }
}
