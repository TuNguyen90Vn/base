package com.tn.base.di.viewmodel

import androidx.lifecycle.ViewModelProvider
import com.tn.base.common.ViewModelFactory
import com.tn.base.di.scopes.ApplicationScope
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {

    @Binds
    @ApplicationScope
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory) : ViewModelProvider.Factory

}