package com.tn.base.di.viewmodel

import androidx.lifecycle.ViewModel
import com.hdk24.basemvvm.di.scope.ViewModelKey
import com.tn.base.presenter.ui.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [ViewModelFactoryModule::class])
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun providesMovieViewModel(viewModel: MainViewModel) : ViewModel

}