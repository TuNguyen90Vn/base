package com.tn.base.domain

import com.tn.base.data.model.transformTo
import com.tn.base.presenter.model.Movie
import com.tn.base.data.remote.MovieApi
import io.reactivex.Single
import javax.inject.Inject

interface MovieRepository {

    fun fetchMovie(page: Int): Single<List<Movie>>

}

class MovieRepositoryImpl @Inject constructor(
    private val api: MovieApi
) : MovieRepository {

    override fun fetchMovie(page: Int): Single<List<Movie>> {
        return api.getMovies(page = page)
            .map {
                it.transformTo()
            }
    }
}