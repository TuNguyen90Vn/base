package com.tn.base.presenter.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie(

    var id: Long = 0L,

    var voteCount: Int,

    var voteAverage: Double = 0.0,

    var title: String = "",

    var popularity: Double = 0.0,

    var posterPath: String = "",

    var originalLanguage: String = "",

    var backdropPath: String = "",

    var overview: String = ""

) : Parcelable
