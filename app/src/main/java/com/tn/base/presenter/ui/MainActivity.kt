package com.tn.base.presenter.ui

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.tn.base.R
import com.tn.base.common.ViewModelFactoryHolder
import com.tn.base.common.injectVM
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), ViewModelFactoryHolder {

    @Inject
    lateinit var vmFactory: ViewModelProvider.Factory

    override val viewModelFactory: ViewModelProvider.Factory
        get() = vmFactory

    private val viewModel: MainViewModel by injectVM()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.test()
    }


}