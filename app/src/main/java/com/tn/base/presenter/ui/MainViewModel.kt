package com.tn.base.presenter.ui

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.ViewModel
import com.tn.base.domain.MovieRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

const val TAG = "MainViewModel"

@SuppressLint("CheckResult")
class MainViewModel @Inject constructor(
    private val repository: MovieRepository
): ViewModel() {

    init {
        Log.d(TAG, "Hello World")
    }

    fun test() {
        repository.fetchMovie(1)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onSuccess = {

                },
                onError = {

                }
            )
    }


}